let links = document.links;
let title;

for (let i=0; i < links.length; i++) {
    let a = links[i];
    if(a.title !== '') {
        a.addEventListener("mouseover", createTips);
        a.addEventListener("mouseover", cancelTips);
    }
    console.log(a)
}

function createTips(ev) {
    let title = this.title;
    this.title = '';
    this.setAttribute("tooltip", title);

    let padding = 5;
    let linkProps = this.getBoundingClientRect();
    let tooltipProps = tooltipWrap.getBoundingClientRect();
    let tosPos = linkProps.top - (tooltipProps.height + padding);
    tooltipWrap.setAttribute('style', `top: ${tosPos}px; left:${linkProps.left}px;`)

    let tooltipWrap = document.createElement("div");
    tooltipWrap.className = 'tooltip';
    tooltipWrap.appendChild(document.createTextNode(title));

    let firstChild = document.firstChild;
    firstChild.parentNode.insertBefore(tooltipWrap, firstChild);
}

function cancelTips(ev) {
    this.title = this.getAttribute("tooltip");
    this.removeAttr("tooltip");

}
