const {sass} = require('laravel-mix');
const mix = require('laravel-mix');
const LiveReloadPlugin = require('webpack-livereload-plugin');

mix.disableNotifications()

sass('assets/sass/styles.scss', 'assets/css/styles.css')

mix.webpackConfig({
    plugins: [
        new LiveReloadPlugin()
    ]
});